// In einem Cookie kann beliebiger Text gespeichert werden.
// Das Auslesen von name=value pairs muss händisch erfolgen.
function setCookie(name, value, days) {
	// Einen für das cookie gültigen String für expires erzeugen
	const date = new Date();
	/*
		aktueller Zeitpunkt in Millisekunden + 60 sek * 1000 (ms) * 60 (min) * 24 (stunden) = 1 Tag * Anzahl der Tage
	*/
	date.setTime(date.getTime() + (1000 * 60 * 60 * 24 * days));
	// Umwandeln in einen für cookies zulässigen String
	const expiry = date.toUTCString();
	// Path sagt, dass das cookie auf der ganzen Site gültig ist
	// Es können cookies auch für Unterverzeichnisse gesetzt werden
	document.cookie = `${name}=${value}; expires=${expiry}; path=/`;
}

setCookie('highscore', 400, 3);
console.log(document.cookie);

/*
	Mehrere Werte in einem Cookie. Der String muss gesplittet werden.
	ACHTUNG: strings werden URIencoded
	Das dummyCookie steht für das Auslesen über document.cookie
*/
let dummyCookie = "highscore=1200;player=Berthold%20Mayer";

// decode dummyCookie. Achtung Cookies werden URI encodiert, sie müsse decodiert werden.
dummyCookie = decodeURIComponent(dummyCookie);

// Zuerst nach Beistrich splitten
const entries = dummyCookie.split(";");
const cookieValues = {};

entries.forEach((el) => {
	const entry = el.split('=');
	cookieValues[entry[0]] = entry[1];

});

console.log(cookieValues);


