/*
	Local Storage erlaubt es uns lokal am Client name value Pairs auch session Übergreifend
	für die aktuelle Domain abzulegen.
*/

// Schreiben von Daten.
// localStorage.setItem('highscore', 1200);
// localStorage.setItem('player', 'Berthold Mayer');
localStorage.setItem(
	"someObject",
	JSON.stringify({
		vorname: "Berthold",
		nachname: "Mayr",
	})
);

// Auslesen von Daten
console.log("Highscore: " + localStorage.getItem("highscore"));
console.log("Player: " + localStorage.getItem("player"));
console.log(JSON.parse(localStorage.getItem("someObject")));

/*
	localStorage.removeItem(key); Einzelnen Eintrag löschen
	localStorage.clear();  alles löschen
*/
